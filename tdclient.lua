local tdlib = require 'tdlib'
local json = require 'json'
local mutex = require 'mutex'
local cond = require 'condition_variable'
local time = require 'time'

local is_receiver_fiber = {}
local inf = 1 / 0

-- get an API key at <https://my.telegram.org/>
local telegram_api_id = '<ID_TEMPLATE>'
local telegram_api_hash = '<API_HASH>'

-- Isso é uma gambiarra não-usada que esqueci de lhe explicar. Me
-- cobre depois.
local json2 = {}
do
    local qt = require 'qt6'
    local qml = qt.load_qml(byte_span.append [[
        import QtQml
        QtObject {
            function json_reencode(v: string): string {
                return JSON.stringify(JSON.parse(v))
            }
        }
    ]])

    json2.encode = function(v)
        return qml.object('json_reencode(QString)', json.encode(v))
    end
end

local updateAuthorizationStateHandlers = {
    authorizationStateWaitTdlibParameters = function(client, v)
        local res = client.execute {
            ['@type'] = 'setTdlibParameters',
            use_test_dc = true,
            --database_directory = '/home/USER/my-tests',
            --files_directory = '/home/USER/my-tests2',
            use_file_database = true,
            use_chat_info_database = true,
            use_message_database = true,
            use_secret_chats = false,
            api_id = telegram_api_id,
            api_hash = telegram_api_hash,
            system_language_code = 'en-US',
            device_model = 'linux-generic',
            application_version = '0.1.7'
        }
        if (res['@type'] == 'error') then
            client.qml.property.statusLabel.text = 'Initialization failed: ' ..
                res.message
        end
    end,
    authorizationStateWaitPhoneNumber = function(client, v)
        local qml = client.qml
        qml.property.loginForm['newLogin(QString)'] = function(phone_number)
            spawn(function()
                local res = client.execute {
                    ['@type'] = 'setAuthenticationPhoneNumber',
                    phone_number = phone_number,
                    allow_flash_call = false,
                    allow_missed_call = false,
                    is_current_phone_number = false,
                    allow_sms_retriever_api = false
                }
                if (res['@type'] == 'error') then
                    qml.property.statusLabel.text = 'Login failed: ' ..
                        res.message
                    qml.property.loginForm.visible = true
                end
            end):detach()
        end
        qml.property.loginForm.visible = true
    end,
    authorizationStateWaitCode = function(client, v)
        local qml = client.qml
        qml.property.smsCodeForm['smsCode(QString)'] = function(sms_code)
            spawn(function()
                local res = client.execute {
                    ['@type'] = 'checkAuthenticationCode',
                    code = sms_code
                }
                if (res['@type'] == 'error') then
                    qml.property.statusLabel.text = 'Login failed: ' ..
                        res.message
                    qml.property.loginForm.visible = true
                end
            end):detach()
        end
        qml.property.smsCodeForm.visible = true
    end,
    authorizationStateReady = function(client, v)
        print('tô pronto')
    end
}

local handlers = {
    updateOption = function(client, v)
        client.state[v.name] = v.value.value
    end,
    updateAuthorizationState = function(client, v)
        local authorization_state = v.authorization_state

        if updateAuthorizationStateHandlers[authorization_state['@type']] then
            updateAuthorizationStateHandlers[authorization_state['@type']](
                client, authorization_state)
        else
            print('unhandled updateAuthorizationState: ' .. json.encode(v))
        end
    end,
    updateAnimationSearchParameters = function(client, v)
    end,
    updateSelectedBackground = function(client, v)
    end,
    updateFileDownloads = function(client, v)
    end,
    updateDefaultReactionType = function(client, v)
    end,
    updateConnectionState = function(client, v)
        client.qml.property.connectionIndicator.running = (
            v.state['@type'] ~= 'connectionStateReady')
    end,
    updateUser = function(client, v)
        --v.user
    end,
    updateNewChat = function(client, v)
    end,
    updateNewMessage = function(client, v)
    end,
    updateSuggestedActions = function(client, v)
    end,
    updateAttachmentMenuBots = function(client, v)
    end,
    updateDiceEmojis = function(client, v)
    end,
    updateHavePendingNotifications = function(client, v)
    end,
    updateChatFolders = function(client, v)
    end,
    updateActiveEmojiReactions = function(client, v)
    end,
    updateChatThemes = function(client, v)
    end,
    updateUserStatus = function(client, v)
    end,
    updateUnreadChatCount = function(client, v)
    end,
    updateScopeNotificationSettings = function(client, v)
    end,
    updateChatReadInbox = function(client, v)
    end,
    updateChatReadOutbox = function(client, v)
    end,
    updateChatLastMessage = function(client, v)
    end,
    updateChatNotificationSettings = function(client, v)
    end
}

function new(qml)
    local td = tdlib.new()
    local waiters = {}
    local client = { td = td, state = {}, waiters = waiters, qml = qml }

    client.execute = function(v)
        if this_fiber.local_[is_receiver_fiber] then
            v['@extra'] = 'recur'
            td:send(json.encode(v))
            local queue = this_fiber.local_[is_receiver_fiber]
            while true do
                local v = td:receive()
                v = json.decode(v)
                if v['@extra'] == 'recur' then
                    return v
                else
                    queue[#queue + 1] = v
                end
            end
        end

        local waiter = { mtx = mutex.new(), cond = cond.new() }
        waiter.mtx:lock()
        for i = 1, inf do
            if not waiters[i] then
                waiters[i] = waiter
                v['@extra'] = i
                td:send(json.encode(v))
                scope(function()
                    scope_cleanup_push(function() waiter.mtx:unlock() end)
                    while not waiter.result do
                        waiter.cond:wait(waiter.mtx)
                    end
                end)
                local res = waiter.result
                waiters[i] = nil
                return res
            end
        end
    end

    spawn(function()
        this_fiber.local_[is_receiver_fiber] = {}
        local function process(v)
            if handlers[v['@type']] then
                handlers[v['@type']](client, v)
            elseif waiters[v['@extra']] then
                local waiter = waiters[v['@extra']]
                waiter.result = v
                waiter.cond:notify_one()
            else
                print('unhandled message: ' .. json.encode(v))
            end
        end

        while true do
            -- this indirection allows recursive receive() calls which
            -- matters for client.execute()
            local local_queue = this_fiber.local_[is_receiver_fiber]
            this_fiber.local_[is_receiver_fiber] = {}
            for k in next, local_queue do
                process(local_queue[k])
            end

            local v = td:receive()
            v = json.decode(v)
            process(v)
        end
    end):detach()

    return client
end
