local system = require 'system'
local json = require 'json'
local qt = require 'qt6'

-- set initial attributes that might affect later paths (e.g. settings path)
qt.load_qml(byte_span.append[[
    import QtQuick
    QtObject {
        Component.onCompleted: {
            Application.name = "simple-telegram-client"
            Application.displayName = "Simple Telegram Client"
            Application.version = "1.0"
            Application.organization = "emilua-org"
            Application.domain = "https://emilua.org/"
        }
    }
]])

local ok, qml = pcall(qt.load_qml, 'main2.qml')
if not ok then
    for _, v in ipairs(qml.errors) do
        print(v)
    end
    error(qml)
end

qml.object['visibleChanged(bool)'] = function(visible)
    if not visible then
        spawn(function() system.exit() end):detach()
    end
end

qml.engine['quit()'] = function()
    spawn(function() system.exit() end):detach()
end

-- we only show the GUI once every callback has been set
qml.object('show()')

--do return end
--local tdclient = require './tdclient'.new(qml)
