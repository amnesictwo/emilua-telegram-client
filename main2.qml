import QtCore
import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

ApplicationWindow {
    id: window

    width: 400
    height: 400

    RowLayout {
        anchors.fill: parent

        Frame {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.horizontalStretchFactor: 4
            Rectangle {
                anchors.fill: parent
                color: "yellow"
            }
        }
        Frame {
            Layout.fillWidth: true
            Layout.horizontalStretchFactor: 5
            Layout.fillHeight: true
            ColumnLayout {
                anchors.fill: parent
                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "green"
                }
                RowLayout {
                    TextArea {
                        background: Rectangle { color: "white" }
                        Layout.fillWidth: true
                        placeholderText: "type here..."
                    }
                    Button {
                        text: "send"
                    }
                }
            }
        }
    }

    footer: Frame {
        RowLayout {
            anchors.fill: parent
            Label {
                id: statusLabel
                Layout.alignment: Qt.AlignRight
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight

                background: Rectangle {
                    color: "red"
                    visible: false
                    opacity: 0.0
                    ParallelAnimation {
                        id: animation
                        SequentialAnimation {
                            ScriptAction {
                                script: statusLabel.background.visible = true;
                            }
                            NumberAnimation {
                                target: statusLabel.background
                                property: "opacity"
                                from: 0.0; to: 1.0
                                duration: 500; easing.type: Easing.OutQuad
                            }
                            NumberAnimation {
                                target: statusLabel.background
                                property: "opacity"
                                from: 1.0; to: 0.0
                                duration: 500; easing.type: Easing.InQuad
                            }
                            ScriptAction {
                                script: statusLabel.background.visible = false;
                            }
                        }
                        SequentialAnimation {
                            ColorAnimation {
                                target: statusLabel; property: "color"
                                from: "black"; to: "white"
                                duration: 500; easing.type: Easing.OutQuad
                            }
                            ColorAnimation {
                                target: statusLabel; property: "color"
                                from: "white"; to: "black"
                                duration: 500; easing.type: Easing.InQuad
                            }
                        }
                    }
                }
                onTextChanged: animation.start()
            }
            BusyIndicator {
                id: connectionIndicator
                Layout.alignment: Qt.AlignRight
                running: false
            }
        }
    }

    Shortcut {
        sequence: "Ctrl+Q"
        onActivated: Qt.quit()
    }

    Settings {
        id: settings

        property alias x: window.x
        property alias y: window.y
        property alias width: window.width
        property alias height: window.height
    }
}
