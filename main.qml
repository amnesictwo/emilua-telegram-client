import QtCore
import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

ApplicationWindow {
    id: window

    width: 400
    height: 400

    Settings {
        id: settings

        property alias x: window.x
        property alias y: window.y
        property alias width: window.width
        property alias height: window.height
    }

    menuBar: MenuBar {
        visible: false
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit()
            }
        }
    }

    footer: Frame {
        RowLayout {
            anchors.fill: parent
            Label {
                id: statusLabel
                Layout.alignment: Qt.AlignRight
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight

                background: Rectangle {
                    color: "red"
                    visible: false
                    opacity: 0.0
                    ParallelAnimation {
                        id: animation
                        SequentialAnimation {
                            ScriptAction {
                                script: statusLabel.background.visible = true;
                            }
                            NumberAnimation {
                                target: statusLabel.background
                                property: "opacity"
                                from: 0.0; to: 1.0
                                duration: 500; easing.type: Easing.OutQuad
                            }
                            NumberAnimation {
                                target: statusLabel.background
                                property: "opacity"
                                from: 1.0; to: 0.0
                                duration: 500; easing.type: Easing.InQuad
                            }
                            ScriptAction {
                                script: statusLabel.background.visible = false;
                            }
                        }
                        SequentialAnimation {
                            ColorAnimation {
                                target: statusLabel; property: "color"
                                from: "black"; to: "white"
                                duration: 500; easing.type: Easing.OutQuad
                            }
                            ColorAnimation {
                                target: statusLabel; property: "color"
                                from: "white"; to: "black"
                                duration: 500; easing.type: Easing.InQuad
                            }
                        }
                    }
                }
                onTextChanged: animation.start()
            }
            BusyIndicator {
                id: connectionIndicator
                Layout.alignment: Qt.AlignRight
                running: false
            }
        }
    }

    Shortcut {
        sequence: "Ctrl+Q"
        onActivated: Qt.quit()
    }

    Dialog {
        id: loginForm

        signal newLogin(phone_number: string)

        modal: true
        standardButtons: DialogButtonBox.Ok | DialogButtonBox.Cancel
        anchors.centerIn: parent

        TextField {
            id: usernameField
            placeholderText: "Phone number"
            width: parent.width
        }

        onRejected: Qt.quit()
        onAccepted: newLogin(usernameField.text)
    }

    Dialog {
        id: smsCodeForm

        signal smsCode(code: string)

        modal: true
        standardButtons: DialogButtonBox.Ok | DialogButtonBox.Cancel
        anchors.centerIn: parent

        TextField {
            id: smsCodeField
            placeholderText: "SMS code"
            width: parent.width
        }

        onRejected: Qt.quit()
        onAccepted: {
            var txt = smsCodeField.text
            smsCodeField.clear()
            smsCode(txt)
        }
    }
}
